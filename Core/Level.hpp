#pragma once
#include "IVerificationControl.hpp"
#include "ISourceControl.hpp"
#include "Control.hpp"

namespace pipes
{
class Level
{
public:
    bool run();
    void addSourceControl(std::shared_ptr<ISourceControl>);
    void addVerficationControl(std::shared_ptr<IVerificationControl>);
    void addControl(std::shared_ptr<Control>);
private:
    SourceControls sourceControls;
    VerificationControls verificationControls;
    Controls controls;
    int iteration = 0;
};
}

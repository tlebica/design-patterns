#pragma once
#include <memory>

class QGridLayout;
class QWidget;

namespace pipes
{
class Level;
std::shared_ptr<Level> createLevel(const std::string&, QGridLayout*, QWidget*);

}

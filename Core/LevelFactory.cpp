#include "LevelFactory.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <QGridLayout>
#include <QWidget>
#include "PipeWidget.hpp"
#include "ControlsFactory.hpp"
#include "Control.hpp"
#include "Level.hpp"

namespace pipes
{

namespace
{
using Line=std::vector<int>;
using ControlPtr = std::shared_ptr<Control>;
class LevelFile
{

public:
    bool open(const std::string& fileName)
    {
        try
        {
            file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
            file.open(fileName);
            Line line = getLine();

            if (line.size() != 2)
            {
                std::cerr << "Incorrect format of rows and columns definitions " << std::to_string(line.size()) << " " << fileName << std::endl;
                return false;
            }
            numOfRows = line[0];
            numOfColumns = line[1];
            int numberOfWidgets = numOfRows*numOfColumns;
            for (int i = 0; i < numberOfWidgets; i++)
            {
                Line&& line = getLine();
                if (line.size() < 9)
                {
                    std::cerr << "Could not read control " << std::to_string(i) << " from file " << fileName << std::endl;
                    return false;
                }
                controls.push_back(line);
            }
        }
        catch (...)
        {
            std::cerr << "Could not read level from file " << fileName << std::endl;
        }
        file.close();
        return true;
    }
    int numOfRows;
    int numOfColumns;
    std::vector<Line> controls;

private:
    Line getLine()
    {
        std::string line;
        if (std::getline( file, line));
        {
            return split(std::move(std::stringstream{line}));
        }
        return Line{};
    }
    Line split(std::stringstream&& ss)
    {
        Line result;
        std::string item;
        while (std::getline(ss, item, ' '))
        {
            result.push_back(atoi(item.data()));
        }
        return result;
    }

    std::ifstream file;
};

void setType(ui::PipeWidget* widget, Line& line)
{
    switch (line[0])
    {
    case 0:
        widget->setType("S");
        break;
    case 1:
        widget->setType("V");
        break;
    case 2:
        widget->setType("P");
        break;
    case 3:
        widget->setType("A");
        break;
    case 4:
        widget->setType("S");
        break;
    case 5:
        widget->setType("N");
        break;
    case 6:
        widget->setType("C");
        break;
    default:
        widget->setType("!");
    }
}

void disableRotation(ui::PipeWidget* widget, Line line)
{
    if (line[1])
    {
        widget->disableRotation();
    }
}

FlowDirections readFlowDirections(Line line)
{
    FlowDirections result;
    for (int i = 0; i< 4; i++)
    {
        Direction direction = static_cast<Direction>(i);
        FlowDirection flowDirection = static_cast<FlowDirection>(line[i+2]);
        result[direction] = flowDirection;
    }
    return result;
}

CMYKColor readColor(Line line)
{
    return CMYKColor{line[6], line[7], line[8]};
}

void setPipeDirections(ui::PipeWidget* widget, Line line)
{
    FlowDirections directions = readFlowDirections(line);
    for(auto& pair : directions)
    {
        widget->setDirection(pair.first, pair.second);
    }
}

ControlPtr createControl(ControlsFactory& factory, IPipeGui& pipe, Line line)
{
    switch (line[0])
    {
    case 0:
        return factory.createSourceControl(pipe, readFlowDirections(line), readColor(line));
    case 1:
        return factory.createVerificationControl(pipe, readFlowDirections(line), readColor(line));
    case 2:
        return factory.createPassControl(pipe, readFlowDirections(line));
    case 3:
        return factory.createAddControl(pipe, readFlowDirections(line));
    case 4:
        return factory.createSubstractControl(pipe, readFlowDirections(line), static_cast<Direction>(line[6]));
    case 5:
        return factory.createNegateControl(pipe, readFlowDirections(line));
    case 6:
        return factory.createConditionalControl(pipe, readFlowDirections(line), readColor(line), static_cast<Direction>(line[9]), static_cast<Direction>(line[10]));
    default:
        return ControlPtr{};
    }
}
}

IPipeGui& createAndPlacePipeWidget(int i, int j, QGridLayout* layout, Line line, QWidget * widget)
{
    ui::PipeWidget* pipe = new ui::PipeWidget(widget);
    setType(pipe, line);
    disableRotation(pipe, line);
    setPipeDirections(pipe, line);
    layout->addWidget(pipe,i,j,1,1);
    return *pipe;
}

std::shared_ptr<Level> createLevel(const std::string & fileName, QGridLayout * layout, QWidget * widget)
{
    LevelFile file;
    std::shared_ptr<Level> level = std::make_shared<Level>();
    ControlsFactory factory{level};
    if (not file.open(fileName))
    {
        return level;
    }

    std::vector<ControlPtr> previousRow;
    std::vector<ControlPtr> currentRow;
    for (int row = 0; row < file.numOfRows; row++)
    {
        previousRow = currentRow;
        currentRow.clear();
        ControlPtr previousControl;
        ControlPtr currentControl;

        for (int column = 0; column < file.numOfColumns; column++)
        {
            previousControl = currentControl;
            Line line = file.controls[row*file.numOfColumns+column];
            IPipeGui& pipe = createAndPlacePipeWidget(row, column, layout, line, widget);
            currentControl = createControl(factory, pipe, line);

            if (previousControl && currentControl)
            {
                previousControl->connect(currentControl, Direction::East);
                currentControl->connect(previousControl, Direction::West);
            }
            currentRow.push_back(currentControl);
        }

        if (previousRow.size() == file.numOfColumns && currentRow.size() == file.numOfColumns)
        {
            for (int column = 0; column < file.numOfColumns; column++)
            {
                if (previousRow[column] && currentRow[column])
                {
                    previousRow[column]->connect(currentRow[column], Direction::South);
                    currentRow[column]->connect(previousRow[column], Direction::North);
                }
            }
        }
    }
    return level;
}

}

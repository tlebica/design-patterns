#include "PipeWidget.hpp"
#include <QPalette>
#include <string>
#include <iostream>

namespace pipes
{
namespace ui
{

void PipeWidget::setUp()
{
    centralWidget = new QGridLayout(this);
    centralWidget->setMargin(0);
    centralWidget->setSpacing(0);
    centralWidget->setContentsMargins(0,0,0,0);
    centralWidget->setHorizontalSpacing(0);
    centralWidget->setVerticalSpacing(0);

    addPipe(Direction::North);
    addPipe(Direction::South);
    addPipe(Direction::East);
    addPipe(Direction::West);

    button = new QPushButton("A",this);
    button->setMaximumSize(32,32);
    button->setMinimumSize(32,32);
    button->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(button,SIGNAL(clicked()),this, SLOT(buttonClicked()));
    connect(button,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(rightButtonClicked(QPoint)));
    centralWidget->addWidget(button, 1,1,1,1);

    centralWidget->setSpacing(0);
    centralWidget->setMargin(0);

    centralWidget->setColumnStretch(0,1);
    centralWidget->setColumnStretch(2,1);
    centralWidget->setRowStretch(0,1);
    centralWidget->setRowStretch(2,1);
}

void PipeWidget::addPipe(Direction pipeType)
{
    setPipe(pipeType, new Pipe(this, pipeType));
    switch (pipeType)
    {
    case Direction::North:
        topWidget = new QWidget(this);
        topLayout = new QHBoxLayout(topWidget);
        topWidget->setLayout(topLayout);
        topLayout->addWidget(getPipe(pipeType));
        topLayout->setSpacing(0);
        topLayout->setMargin(0);
        centralWidget->addWidget(topWidget,0,1,1,1);
        break;
    case Direction::South:
        bottomWidget = new QWidget(this);
        bottomLayout = new QHBoxLayout(bottomWidget);
        bottomWidget->setLayout(bottomLayout);
        bottomLayout->addWidget(getPipe(pipeType));
        bottomLayout->setSpacing(0);
        bottomLayout->setMargin(0);
        centralWidget->addWidget(bottomWidget,2,1,1,1);
        break;
    case Direction::East:
        centralWidget->addWidget(getPipe(pipeType),1,2,1,1);
        break;
    case Direction::West:
        centralWidget->addWidget(getPipe(pipeType),1,0,1,1);
        break;
    }
}

void PipeWidget::updateDisabledEnabledState()
{
    for (int i = 0; i < 4; i++)
    {
        pipes[i]->setDisabled(disabled);
    }
}

FlowDirection PipeWidget::getDirection(Direction p)
{
    return getPipe(p)->getDirection();
}

void PipeWidget::setDirection(Direction p , FlowDirection d)
{
    getPipe(p)->setDirection(d);
}

void PipeWidget::setColor(Direction p , IRGBColor &&c)
{
    getPipe(p)->setColor(c.getRed(),c.getGreen(),c.getBlue());
}

void PipeWidget::setButtonColor(IRGBColor &&c)
{
    setButtonColor(QColor(c.getRed(),c.getGreen(),c.getBlue()));
}

void PipeWidget::resetPipes()
{
    for (int i = 0; i < 4; i++)
    {
        pipes[i]->setBaseColor();
    }
    setButtonColor(palette().color(QPalette::Background));
}

void PipeWidget::registerRotateCallback(RotateCallback&& callback)
{
    rotateCallback = callback;
}

void PipeWidget::registerToggleEnabledCallback(ToggleEnabledCallback&& callback)
{
    toggleEnabledCallback = callback;
}

void PipeWidget::buttonClicked()
{
    if (canRotate)
    {
        rotate();
    }
}

void PipeWidget::rightButtonClicked(QPoint)
{
    disabled = not disabled;
    if (toggleEnabledCallback)
    {
        toggleEnabledCallback();
    }
    updateDisabledEnabledState();
}

void PipeWidget::setButtonColor(QColor color)
{
    QPalette pal(button->palette());
    pal.setColor(QPalette::Button, color);
    button->setPalette(pal);
    button->setAutoFillBackground(true);
    button->update();
}

void PipeWidget::rotate()
{
    FlowDirection direction = pipes[0]->getDirection();

    for (int i = 0; i < 3; i++)
    {
        pipes[i]->setDirection(pipes[i+1]->getDirection());
    }

    pipes[3]->setDirection(direction);
    if(rotateCallback)
    {
        rotateCallback();
    }
}

void PipeWidget::disable()
{
    disabled = false;
    updateDisabledEnabledState();
}

void PipeWidget::disableRotation()
{
    canRotate = false;
}

void PipeWidget::setType(const char* text)
{
    button->setText(text);
}

void PipeWidget::enable()
{
    disabled = false;
    updateDisabledEnabledState();
}


}
}

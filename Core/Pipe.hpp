#pragma once
#include <QWidget>
#include <QGridLayout>
#include <QTextEdit>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QBoxLayout>
#include <QFrame>
#include <memory>
#include "Definitions.hpp"

namespace pipes
{
namespace ui
{

class Pipe : public QWidget
{
    Q_OBJECT
public:
    Pipe(QWidget* parent, Direction pipeType)
        :QWidget(parent)
    {
        setUp(pipeType);
    }

public:
    void setDirection(FlowDirection);
    FlowDirection getDirection();
    void setColor(int red, int green, int blue);
    void setBaseColor();
private:

    void setUp(Direction);
    void setColor(QColor);
    QColor getColor();

    QFrame* frame;
    QGridLayout* centralLayout;
    QLayout* layout;
    QLabel* pipeIn;
    QLabel* pipeOut;
    std::unique_ptr<QPixmap> incomming;
    std::unique_ptr<QPixmap> outgoing;
    std::unique_ptr<QPixmap> empty;
    FlowDirection direction = FlowDirection::None;
    QColor color;
};
}
}

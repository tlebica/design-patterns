#include "Level.hpp"

namespace pipes
{
bool Level::run()
{
    for(auto& c : controls)
    {
        c->reset();
    }
    for(auto& s : sourceControls)
    {
        s->start(iteration++);
    }
    bool result = verificationControls.size() != 0;
    for(auto& v : verificationControls)
    {
        result = result && v->verify();
    }
    return result;
}

void Level::addSourceControl(std::shared_ptr<ISourceControl> s)
{
    sourceControls.push_back(s);
}

void Level::addVerficationControl(std::shared_ptr<IVerificationControl> v)
{
    verificationControls.push_back(v);
}

void Level::addControl(std::shared_ptr<Control> c)
{
    controls.push_back(c);
}
}

#pragma once

#include <QMainWindow>
#include "PipeWidget.hpp"
#include <memory>

namespace pipes
{
struct Level;
namespace ui
{


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

private:
    void loadLevel(int i);
    void handleTimer();
    void resetPipes();
    std::shared_ptr<Level> level;
    QWidget* centralWidget = nullptr;
    QGridLayout* layout = nullptr;
    QLabel* statusLabel = nullptr;
    int iteration = 0;
};
}
}

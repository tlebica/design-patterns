#include "MainWindow.hpp"
#include "LevelFactory.hpp"
#include "Level.hpp"
#include <QTimer>
#include <iostream>
#include <QMenuBar>
#include <QStatusBar>

namespace pipes
{
namespace ui
{

MainWindow::MainWindow(QWidget *parent)
    :QMainWindow(parent)
{
    QMenu* levelMenu = menuBar()->addMenu("Level");

    for (int i = 1; i < 11; i++)
    {
        std::string actionName = "Level ";
        actionName += std::to_string(i);
        QAction * levelAction = new QAction(actionName.data(), parent);
        connect(levelAction, &QAction::triggered, [this,i](){this->loadLevel(i);});
        levelMenu->addAction(levelAction);
    }

    statusLabel = new QLabel(statusBar());
    statusLabel->setAlignment(Qt::AlignHCenter);
    statusBar()->addPermanentWidget(statusLabel, 1);
    statusBar()->setSizeGripEnabled(false);

    loadLevel(1);

    handleTimer();
}

void MainWindow::loadLevel(int i)
{
    delete centralWidget;
    centralWidget = new QWidget(this);
    layout = new QGridLayout(centralWidget);
    layout->setSpacing(0);
    layout->setMargin(0);
    centralWidget->setLayout(layout);

    std::string levelFile = "Level";
    levelFile += std::to_string(i);
    levelFile += ".txt";
    level = createLevel(levelFile,layout,centralWidget);

    setCentralWidget(centralWidget);
    statusLabel->setText("Try to solve this one!");

    this->adjustSize();
    std::string windowTitle = "pipes - Level ";
    windowTitle += std::to_string(i);
    this->setWindowTitle(windowTitle.data());
}

void MainWindow::handleTimer()
{
    QTimer::singleShot(100,this,&MainWindow::handleTimer);
    resetPipes();
    if(level->run())
    {
        statusLabel->setText("Level completed!");
    }
}

void MainWindow::resetPipes()
{
    for ( int i = 0; i < layout->count(); i++)
    {
        QLayoutItem* item = layout->itemAt(i);
        QWidget* widget = item->widget();
        if (widget)
        {
            PipeWidget* pipe = dynamic_cast<PipeWidget*>(widget);
            pipe->resetPipes();
        }
    }
}

}
}

#pragma once
#include "Pipe.hpp"
#include "IRGBColor.hpp"
#include <QWidget>
#include <QGridLayout>
#include <QTextEdit>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QFrame>
#include "Definitions.hpp"
#include "IPipeGui.hpp"

namespace pipes
{
namespace ui
{
class PipeWidget : public QFrame, public IPipeGui
{
    Q_OBJECT
public:
    PipeWidget(QWidget* parent)
        :QFrame(parent)
    {
        setUp();
    }

    void rotate();
    void disable();
    void enable();
    void disableRotation();
    void setType(const char *);
    FlowDirection getDirection(Direction);
    void setDirection(Direction, FlowDirection);
    void setColor(Direction, IRGBColor&&) override;
    void setButtonColor(IRGBColor&&) override;
    void resetPipes();
    void registerRotateCallback(RotateCallback&& callback) override;
    void registerToggleEnabledCallback(ToggleEnabledCallback&& callback) override;
private slots:
    void buttonClicked();
    void rightButtonClicked(QPoint);

private:
    void setButtonColor(QColor);
    void setUp();
    void addPipe(Direction);
    Pipe* getPipe(Direction p)
    {
        return pipes[static_cast<int>(p)];
    }

    void setPipe(Direction t, Pipe* p)
    {
        pipes[static_cast<int>(t)] = p;
    }
    void updateDisabledEnabledState();

    QGridLayout* centralWidget;
    QPushButton* button;
    Pipe* northPipe;
    Pipe* eastPipe;
    Pipe* southPipe;
    Pipe* westPipe;
    Pipe* pipes[4];
    QWidget* topWidget;
    QHBoxLayout* topLayout;
    QWidget* bottomWidget;
    QHBoxLayout* bottomLayout;
    bool disabled = false;
    bool canRotate = true;
    RotateCallback rotateCallback;
    ToggleEnabledCallback toggleEnabledCallback;
};
}
}

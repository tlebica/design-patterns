#include "Pipe.hpp"

namespace pipes
{
namespace ui
{
void Pipe::setUp(Direction pipeType)
{
    frame = new QFrame(this);
    frame->setFrameShape(QFrame::Box);
    centralLayout = new QGridLayout(this);
    setLayout(centralLayout);
    centralLayout->addWidget(frame,0,0,1,1);
    centralLayout->setSpacing(0);
    centralLayout->setMargin(0);

    pipeIn = new QLabel(this);
    pipeOut = new QLabel(this);
    empty.reset(new QPixmap(0,0));
    QPixmap arrow1("arrow.png");
    QPixmap arrow = arrow1.copy(0,8,32,16);
    QTransform incomingTransform;
    switch (pipeType)
    {
    case Direction::North:
        layout = new QVBoxLayout(frame);
        pipeOut->setMinimumWidth(16);
        pipeOut->setMaximumWidth(16);
        pipeIn->setMinimumWidth(16);
        pipeIn->setMaximumWidth(16);
        pipeIn->setMaximumHeight(32);
        pipeIn->setMinimumHeight(32);
        pipeOut->setMaximumHeight(QWIDGETSIZE_MAX);
        pipeOut->setMinimumHeight(0);
        layout->addWidget(pipeOut);
        layout->addWidget(pipeIn);
        incomingTransform.rotate(90);
        setMaximumWidth(18);
        break;
    case Direction::South:
        layout = new QVBoxLayout(frame);
        pipeOut->setMinimumWidth(16);
        pipeOut->setMaximumWidth(16);
        pipeIn->setMinimumWidth(16);
        pipeIn->setMaximumWidth(16);
        pipeIn->setMaximumHeight(32);
        pipeIn->setMinimumHeight(32);
        pipeOut->setMaximumHeight(QWIDGETSIZE_MAX);
        pipeOut->setMinimumHeight(0);

        layout->addWidget(pipeIn);
        layout->addWidget(pipeOut);
        incomingTransform.rotate(270);
        setMaximumWidth(18);
        break;
    case Direction::East:
        layout = new QHBoxLayout(frame);
        pipeOut->setMinimumHeight(16);
        pipeOut->setMaximumHeight(16);
        pipeIn->setMinimumHeight(16);
        pipeIn->setMaximumHeight(16);
        pipeIn->setMaximumWidth(32);
        pipeIn->setMinimumWidth(32);
        pipeOut->setMaximumWidth(QWIDGETSIZE_MAX);
        pipeOut->setMinimumWidth(0);

        layout->addWidget(pipeIn);
        layout->addWidget(pipeOut);
        incomingTransform.rotate(180);
        setMaximumHeight(18);
        break;
    case Direction::West:
        layout = new QHBoxLayout(frame);
        pipeOut->setMinimumHeight(16);
        pipeOut->setMaximumHeight(16);
        pipeIn->setMinimumHeight(16);
        pipeIn->setMaximumHeight(16);
        pipeIn->setMaximumWidth(32);
        pipeIn->setMinimumWidth(32);
        pipeOut->setMaximumWidth(QWIDGETSIZE_MAX);
        pipeOut->setMinimumWidth(0);

        layout->addWidget(pipeOut);
        layout->addWidget(pipeIn);
        incomingTransform.rotate(0);
        setMaximumHeight(18);
        break;
    }
    frame->setLayout(layout);
    layout->setSpacing(0);
    layout->setMargin(0);
    layout->setContentsMargins(0,0,0,0);
    incomming.reset(new QPixmap(arrow.transformed(incomingTransform)));
    QTransform outgoingTransform;
    outgoingTransform.rotate(180);
    outgoing.reset(new QPixmap(incomming->transformed(outgoingTransform)));

}

void Pipe::setColor(QColor color)
{
    this->color = color;
    QPalette pal(palette());
    pal.setColor(QPalette::Background, color);
    pipeIn->setAutoFillBackground(true);
    pipeIn->setPalette(pal);
    pipeOut->setAutoFillBackground(true);
    pipeOut->setPalette(pal);
}

QColor Pipe::getColor()
{
    return color;
}

void Pipe::setColor(int red, int green, int blue)
{
    setColor(QColor(red,green,blue));
}

void Pipe::setBaseColor()
{
    setColor(palette().color(QPalette::Background));
}

void Pipe::setDirection(FlowDirection pipeDirection)
{
    direction = pipeDirection;
    setBaseColor();
    switch (pipeDirection)
    {
    case FlowDirection::Incomming:
        pipeIn->setPixmap(*(incomming.get()));
        break;
    case FlowDirection::Outgoing:
        pipeIn->setPixmap(*(outgoing.get()));
        break;
    case FlowDirection::None:
        pipeIn->setPixmap(*(empty.get()));
        break;
    }
}

FlowDirection Pipe::getDirection()
{
    return direction;
}

}
}


#include "sourcecontrol.hpp"
#include "IRGBColor.hpp"
#include "CMYK2RGB.hpp"

namespace pipes
{

struct RGBColor : public ui::IRGBColor
{
    std::uint8_t getRed() override
    {
        return red;
    }
    std::uint8_t getGreen() override
    {
        return green;
    }
    std::uint8_t getBlue() override
    {
        return blue;
    }
    std::uint8_t red;
    std::uint8_t green;
    std::uint8_t blue;
};

void SourceControl::start(int iteration)
{
    RGBColor rgbColor;
    rgbColor.red = getRed(color);
    rgbColor.green = getGreen(color);
    rgbColor.blue = getBlue(color);
    gui.setButtonColor(std::move(rgbColor));
    for (auto direction : outgoingDirections)
    {
        gui.setColor(direction, std::move(rgbColor));
        sendTo(direction, color, iteration);
    }
}

void SourceControl::setColor(CMYKColor color)
{
    this->color = color;
}

void SourceControl::addOutgoingDirection(Direction direction)
{
    outgoingDirections.insert(direction);
}

}

#include "ControlsFactory.hpp"
#include "Level.hpp"
#include "sourcecontrol.hpp"

namespace pipes
{
std::shared_ptr<Control> ControlsFactory::createSourceControl(IPipeGui & gui, FlowDirections directions, CMYKColor color)
{
    std::shared_ptr<SourceControl> control = std::make_shared<SourceControl>(gui);

    control->setColor(color);
    for(auto& pair: directions)
    {
        if (pair.second == FlowDirection::Outgoing)
        {
            control->addOutgoingDirection(pair.first);
        }
    }
    level->addSourceControl(control);
    level->addControl(control);

    return control;
}

std::shared_ptr<Control> ControlsFactory::createVerificationControl(IPipeGui & gui, FlowDirections directions, CMYKColor color)
{
    return std::make_shared<Control>();
}

std::shared_ptr<Control> ControlsFactory::createPassControl(IPipeGui & gui, FlowDirections directions)
{
    return std::make_shared<Control>();
}

std::shared_ptr<Control> ControlsFactory::createAddControl(IPipeGui & gui, FlowDirections directions)
{
    return std::make_shared<Control>();
}

std::shared_ptr<Control> ControlsFactory::createSubstractControl(IPipeGui &, FlowDirections, Direction sub)
{
    return std::make_shared<Control>();
}

std::shared_ptr<Control> ControlsFactory::createNegateControl(IPipeGui &, FlowDirections)
{
    return std::make_shared<Control>();
}

std::shared_ptr<Control> ControlsFactory::createConditionalControl(IPipeGui &, FlowDirections, CMYKColor, Direction control, Direction trueBranch)
{
    return std::make_shared<Control>();
}
}

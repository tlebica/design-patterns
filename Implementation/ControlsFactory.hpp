#pragma once
#include <memory>
#include "Definitions.hpp"
#include "ISourceControl.hpp"
#include "IVerificationControl.hpp"
#include "CMYKColor.hpp"
#include "IPipeGui.hpp"
#include <map>

namespace pipes
{
class Control;
class Level;
using FlowDirections=std::map<Direction, FlowDirection>;

class ControlsFactory
{
public:
    ControlsFactory(std::shared_ptr<Level> level)
        :level(level)
    {}
    std::shared_ptr<Control> createSourceControl(IPipeGui&, FlowDirections, CMYKColor);
    std::shared_ptr<Control> createVerificationControl(IPipeGui&, FlowDirections, CMYKColor);
    std::shared_ptr<Control> createPassControl(IPipeGui&, FlowDirections);
    std::shared_ptr<Control> createAddControl(IPipeGui&, FlowDirections);
    std::shared_ptr<Control> createSubstractControl(IPipeGui&, FlowDirections, Direction sub);
    std::shared_ptr<Control> createNegateControl(IPipeGui&, FlowDirections);
    std::shared_ptr<Control> createConditionalControl(IPipeGui&, FlowDirections, CMYKColor, Direction control, Direction trueBranch);
private:
    std::shared_ptr<Level> level;
};
}

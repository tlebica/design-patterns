#pragma once
#include "ISourceControl.hpp"
#include "Control.hpp"
#include "CMYKColor.hpp"
#include "IPipeGui.hpp"
#include <set>

namespace pipes
{
class SourceControl : public Control, public ISourceControl
{
public:
    SourceControl(IPipeGui& gui)
        :gui(gui)
    {}
    void start(int) override;
    void setColor(CMYKColor);
    void addOutgoingDirection(Direction);
private:
    CMYKColor color = {0,0,0};
    std::set<Direction> outgoingDirections;
    IPipeGui& gui;
};
}


#include "MainWindow.hpp"
#include <QApplication>
#include <iostream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    pipes::ui::MainWindow w;
    w.show();

    return a.exec();
}


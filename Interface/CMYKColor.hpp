#pragma once

namespace pipes
{
struct CMYKColor
{
    int cyan;
    int magenta;
    int yellow;
};
}

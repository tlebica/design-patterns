#pragma once
#include <vector>
#include <memory>

namespace pipes
{
struct IVerificationControl
{
    virtual bool verify() = 0;
    virtual void reset() = 0;
    virtual ~IVerificationControl(){}
};

using VerificationControls=std::vector<std::shared_ptr<IVerificationControl>>;
}

#pragma once
#include <cstdint>

namespace pipes
{
namespace ui
{
class IRGBColor
{
public:
    virtual std::uint8_t getRed() = 0;
    virtual std::uint8_t getGreen() = 0;
    virtual std::uint8_t getBlue() = 0;
    virtual ~IRGBColor(){}
};
}
}

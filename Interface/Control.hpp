#pragma once
#include <memory>
#include "CMYKColor.hpp"
#include "Definitions.hpp"
#include <map>
#include <vector>

namespace pipes
{
class Control : public std::enable_shared_from_this<Control>
{
public:
    void connect(std::shared_ptr<Control> control, Direction direction);
    virtual void reset();
    virtual ~Control(){}
protected:
    void sendTo(Direction direction, CMYKColor color, int iteration);
    virtual void receive(Direction direction, CMYKColor color, int iteration);
private:
    std::map<Direction, std::shared_ptr<Control>> neighbours;
};
using Controls=std::vector<std::shared_ptr<Control>>;
}

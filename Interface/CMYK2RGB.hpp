#pragma once
#include <cstdint>

namespace pipes
{
struct CMYKColor;

std::uint8_t getRed(CMYKColor&);
std::uint8_t getGreen(CMYKColor&);
std::uint8_t getBlue(CMYKColor&);

int getCyan(int red);
int getMagenta(int green);
int getYellow(int blue);

}

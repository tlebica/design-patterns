#include "Definitions.hpp"

namespace pipes
{
Direction getOppositeDirection(Direction original)
{
    switch (original) {
    case Direction::North:
        return Direction::South;
    case Direction::South:
        return Direction::North;
    case Direction::East:
        return Direction::West;
    case Direction::West:
        return Direction::East;
    }
}
}

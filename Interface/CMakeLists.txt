project(interface)

cmake_minimum_required(VERSION 2.8)
aux_source_directory(. SRC_LIST)
add_definitions("-std=c++11 -Ofast")
add_library(${PROJECT_NAME} ${SRC_LIST})


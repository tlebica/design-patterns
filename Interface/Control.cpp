#include "Control.hpp"

namespace pipes
{
void Control::connect(std::shared_ptr<Control> control, Direction direction)
{
    neighbours[direction] = control;
}

void Control::reset()
{

}

void Control::receive(Direction direction, CMYKColor color, int iteration)
{

}

void Control::sendTo(Direction direction, CMYKColor color, int iteration)
{
    if (neighbours.find(direction) != neighbours.end())
    {
        neighbours[direction]->receive(getOppositeDirection(direction), color, iteration);
    }
}

}

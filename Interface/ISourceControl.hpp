#pragma once
#include <vector>
#include <memory>

namespace pipes
{
struct ISourceControl
{
    virtual void start(int iteration) = 0;
    virtual ~ISourceControl(){}
};

using SourceControls=std::vector<std::shared_ptr<ISourceControl>>;
}

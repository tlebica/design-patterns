#pragma once

namespace pipes
{
enum class Direction: int
{
    North = 3,
    East = 2,
    South = 1,
    West = 0
};

enum class FlowDirection: int
{
    None,
    Incomming,
    Outgoing
};

Direction getOppositeDirection(Direction original);

}

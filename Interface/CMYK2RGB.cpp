#include "CMYK2RGB.hpp"
#include "CMYKColor.hpp"
#include <limits>
#include <cmath>

namespace pipes
{
namespace
{
const std::uint8_t MAX_RGB_VALUE = std::numeric_limits<std::uint8_t>::max();
const int MAX_CMYK_VALUE = 300;
const int MIN_CMYK_VALUE = 0;
double getCappedCMYKValue(double value)
{
    return std::fmax(std::fmin(value, MAX_CMYK_VALUE), MIN_CMYK_VALUE);
}

std::uint8_t getCappedRGBValue(double value)
{
    return std::fmax(std::fmin(value, MAX_RGB_VALUE), MIN_CMYK_VALUE);
}

std::uint8_t getRGBValue(double value)
{
    return static_cast<std::uint8_t>(MAX_RGB_VALUE*(MAX_CMYK_VALUE - getCappedCMYKValue(value))/MAX_CMYK_VALUE);
}

int getCMYKValue(int value)
{
    return static_cast<int>(MAX_CMYK_VALUE*(MAX_RGB_VALUE - getCappedRGBValue(value))/MAX_RGB_VALUE);
}
}

std::uint8_t getRed(pipes::CMYKColor & color)
{
    return getRGBValue(color.cyan);
}

std::uint8_t getGreen(pipes::CMYKColor & color)
{
    return getRGBValue(color.magenta);
}

std::uint8_t getBlue(pipes::CMYKColor & color)
{
    return getRGBValue(color.yellow);
}

int getCyan(int red)
{
    return getCMYKValue(red);
}

int getMagenta(int green)
{
    return getCMYKValue(green);
}

int getYellow(int blue)
{
    return getCMYKValue(blue);
}

}

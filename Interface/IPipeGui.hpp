#pragma once
#include <functional>
#include "Definitions.hpp"

namespace pipes
{
namespace ui
{
class IRGBColor;
}
using RotateCallback=std::function<void()>;
using ToggleEnabledCallback=std::function<void()>;
struct IPipeGui
{
    virtual void setColor(Direction, ui::IRGBColor&&) = 0;
    virtual void setButtonColor(ui::IRGBColor&&) = 0;
    virtual void registerRotateCallback(RotateCallback&& callback) = 0;
    virtual void registerToggleEnabledCallback(ToggleEnabledCallback&& callback) = 0;
    virtual ~IPipeGui(){}
};
}
